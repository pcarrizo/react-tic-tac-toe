import React from 'react';

import StepButton from '../components/StepButton';
import { boardCoords, winnerLines } from '../util/constants';

export function getMoves(memory, currentStep, coords, jumpTo) {
  return memory.map((step, move) => {
    const coord = coords[move - 1],
      description = move ? `Go to move #${move} (${coord})` : 'Go to game start';
    return (
      <StepButton 
        key={move} 
        move={move} 
        step={currentStep} 
        description={description} 
        jumpTo={jumpTo} 
      />
    );
  });
}

export function calculateWinner(squares) {
  for (let line of winnerLines) {
    const [a, b, c] = line;
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return {
        symbol: squares[a],
        line: [a, b, c],
      };
    }
  }
  return null;
}

export function setStatus(squares, winner, xIsNext) {
  let status = `Next player: ${xIsNext ? 'X' : 'O'}`;
  if (winner) {
    status = `Winner: ${winner}`;
  }
  return status;
}

export function positionToCoords(position) {
  return boardCoords[position];
}