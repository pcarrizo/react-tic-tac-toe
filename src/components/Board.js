import React from 'react';
import PropTypes from 'prop-types';

import Square from './Square';
import { boardSquares } from '../util/constants';

const Board = (props) => {
  return (
    <div>
      {
        boardSquares.map((row, index) => {
          let bias = index * 3;

          return (
            <div key={index} className="board-row">
            {
              row.map((square, index) => {
                const isWinner = typeof props.winnerLine !== "undefined" && props.winnerLine.indexOf(square) !== -1,
                  position = index + bias;

                return (
                  <Square 
                    key={position} 
                    position={position}
                    value={props.squares[position]}
                    featured={isWinner}
                    onClick={props.onClick}
                  />
                );
              })
            }
            </div>
          );
        })
      }
    </div>
  );
}

Board.propTypes = {
  winnerLine: PropTypes.array,
  squares: PropTypes.array.isRequired,
  onClick: PropTypes.func.isRequired,
}

export default Board;