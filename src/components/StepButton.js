import React from 'react';
import PropTypes from 'prop-types';

class StepButton extends React.Component {
  handleClick = () => {
    this.props.jumpTo(this.props.move);
  }

  render() {
    const className = this.props.step === this.props.move ? "bold" : "";

    return (
      <li>
        <button className={className} onClick={this.handleClick}>
          {this.props.description}
        </button>
      </li>
    );
  }
}

StepButton.propTypes = {
  move: PropTypes.number.isRequired,
  step: PropTypes.number.isRequired,
  description: PropTypes.string.isRequired,
  jumpTo: PropTypes.func.isRequired,
}

export default StepButton;