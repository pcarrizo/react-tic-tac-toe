import React from 'react';
import PropTypes from 'prop-types';

function Info(props) {
  return (
    <div>
      <div>
        {props.status}
      </div>
      <button onClick={props.switchOrder}>
        Toggle moves order
      </button>
      <ol>
        {props.moves}
      </ol>
    </div>
  );
}

Info.propTypes = {
  status: PropTypes.string.isRequired,
  moves: PropTypes.array.isRequired,
  switchOrder: PropTypes.func.isRequired,
}

export default Info;