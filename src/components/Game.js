import React from 'react';

import Board from './Board';
import Info from './Info';
import { getMoves, calculateWinner, setStatus, positionToCoords } from '../util/helpers';

class Game extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      memory: [{ squares: Array(9).fill(null), }],
      step: 0,
      coordsByStep: [],
      xIsNext: true,
      byDescending: false,
    };
  }

  handleClick = (index) => {
    const memory = this.state.memory.slice(0, this.state.step + 1),
      squares = memory[memory.length - 1].squares.slice(),
      coords = this.state.coordsByStep.slice(0, this.state.step);

    if (squares[index] || calculateWinner(squares)) {
      return;
    }

    squares[index] = this.state.xIsNext ? 'X' : 'O';
    this.setState({ 
      memory: memory.concat([{ squares, }]), 
      step: memory.length,
      coordsByStep: coords.concat([ positionToCoords(index) ]),
      xIsNext: !this.state.xIsNext 
    });
  }

  jumpTo = (step) => {
    this.setState({ step, xIsNext: (step % 2) === 0, });
  }

  switchOrder = () => {
    this.setState({ byDescending: !this.state.byDescending, });
  }

  render() {
    const squares = this.state.memory[this.state.step].squares,
      winner = calculateWinner(squares) || {},
      status = setStatus(squares, winner.symbol, this.state.xIsNext),
      moves = getMoves(this.state.memory, this.state.step, this.state.coordsByStep, this.jumpTo);

    return (
      <div className="game">
        <div className="game-board">
          <Board 
            squares={squares} 
            winnerLine={winner.line} 
            onClick={this.handleClick} 
          />
        </div>
        <div className="game-info">
          <Info 
            status={status} 
            moves={this.state.byDescending ? moves.reverse() : moves} 
            switchOrder={this.switchOrder} 
          />
        </div>
      </div>
    );
  }
}

export default Game;