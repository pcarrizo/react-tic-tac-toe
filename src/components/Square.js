import React from 'react';
import PropTypes from 'prop-types';

class Square extends React.Component {
  handleClick = () => {
    this.props.onClick(this.props.position);
  }

  render() {
    const className = this.props.featured ? "square featured" : "square";

    return (
      <button className={className} onClick={this.handleClick}>
        {this.props.value}
      </button>
    );
  }
}

Square.propTypes = {
  position: PropTypes.number.isRequired,
  featured: PropTypes.bool.isRequired,
  value: PropTypes.string,
  onClick: PropTypes.func.isRequired,
}

export default Square;